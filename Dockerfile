FROM docker.elastic.co/beats/filebeat:6.3.1

USER root
ADD https://raw.githubusercontent.com/logzio/public-certificates/master/COMODORSADomainValidationSecureServerCA.crt /etc/pki/tls/certs/
COPY filebeat.yml /usr/share/filebeat/filebeat.yml
RUN chmod 0644 /etc/pki/tls/certs/COMODORSADomainValidationSecureServerCA.crt /usr/share/filebeat/filebeat.yml
